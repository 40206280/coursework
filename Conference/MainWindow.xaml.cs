﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Conference
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
        }
        Attendee p1 = new Attendee();

        private void Fnametxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
           

        }

        private void Snametxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Attreftxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Inametxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Cnametxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void RegtypeCombobx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            
        }

        private void Titletxtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Paidcheckbx_Checked(object sender, RoutedEventArgs e)
        {

            
        }
        private void Presentercheckbx_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void setbtn_Click(object sender, RoutedEventArgs e)
        {
            p1.Fname = Fnametxtbx.Text;
            p1.Sname = Snametxtbx.Text;
            p1.Attref = Attreftxtbx.Text;
            p1.Iname = Inametxtbx.Text;
            p1.Cname = Cnametxtbx.Text;
            p1.Regtype = RegtypeCombobx.Text;
            p1.Title = Titletxtbx.Text;
            p1.Paid = Paidcheckbx.IsChecked.Value;
            p1.Present = Presentercheckbx.IsChecked.Value;

        }

        private void clearbtn_Click(object sender , RoutedEventArgs e)
        {
            Fnametxtbx.Text = String.Empty;
            Snametxtbx.Text = String.Empty;
            Attreftxtbx.Text = String.Empty;
            Inametxtbx.Text = String.Empty;
            Cnametxtbx.Text = String.Empty;
            Titletxtbx.Text = String.Empty;
            Paidcheckbx.IsChecked = false;
            Presentercheckbx.IsChecked = false;

        }

        private void getbtn_Click(object sender, RoutedEventArgs e)
        {
            Fnametxtbx.Text = p1.Fname;
            Snametxtbx.Text = p1.Sname;
            Attreftxtbx.Text = p1.Attref;
            Inametxtbx.Text = p1.Iname;
            Cnametxtbx.Text = p1.Cname;
            RegtypeCombobx.Text = p1.Regtype;
            Titletxtbx.Text = p1.Title;
            Presentercheckbx.IsChecked = p1.Present;
            Paidcheckbx.IsChecked = p1.Paid;

        }

        
        private void invbtn_Click(object sender, RoutedEventArgs e)
        {
            Window1 invoice = new Window1();
            invoice.Show(ref p1);
            this.Close();
            
            
        }

       

        private void certbtn_Click(object sender, RoutedEventArgs e)
        {
            certificate window2 = new certificate();
            window2.Show(ref p1);
            this.Close();
        }
    }

}
