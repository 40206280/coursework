﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conference
{
    public class Attendee
    {
        private string attref;
        public string Attref
        {
            get { return this.attref; }
            set { this.attref = value; }
        }

        private string fname;
        public string Fname
        {
            get { return this.fname; }
            set { this.fname = value; }
        }

        private string sname;
        public string Sname
        {
            get { return this.sname; }
            set { this.sname = value; }

        }

        private string iname;
        public string Iname
        {
            get { return this.iname; }
            set { this.iname = value; }

        }

        private string cname;
        public string Cname
        {
            get { return this.cname; }
            set { this.cname = value; }

        }

        private string regtype;
        public string Regtype
        {
            get { return this.regtype; }
            set { this.regtype = value; }

        }

        private Boolean paid;
        public Boolean Paid
        {
            get { return this.paid; }
            set { this.paid = value; }
        }

        private Boolean present;
        public Boolean Present
        {
            get { return this.present; }
            set { this.present = value; }
        }

        private string title;
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }


        
        public double GetCost()
        {
            double Cost = 0;
            if (Regtype == "Full")
            {
                Cost = 500;
            }
            else if (Regtype == "Student")
            {
                Cost = 300;
            }
            else if (Regtype == "Organiser")
            {
                Cost = 0;
            }
            if (Present == true)
            {
                Cost = Cost / 100 * 90;
            }
            return Cost;
            

        }

        
    

        
        

        

        

    }

 }



