﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Conference
{
    /// <summary>
    /// Interaction logic for certificate.xaml
    /// </summary>
    public partial class certificate : Window
    {
        public certificate()
        {
            InitializeComponent();
        }

        public void Show(ref Attendee p1)
        {
            Label my_label = new Label();
            my_label.Name = "my_label";
            string the_label;
            if (p1.Present)
            {
                the_label = string.Format("This is to certify {0} {1} attended {3} and presented a paper entitled {4}", p1.Fname, p1.Sname, p1.Cname, p1.Title);
            }
            else
            {
                the_label = string.Format("This is to certify {0} {1} attended {3}", p1.Fname, p1.Sname, p1.Cname);
            }
            Grid.SetRow(my_label, 0);
            Grid.SetColumn(my_label, 0);
            certgrid.Children.Add(my_label);
            base.Show();
        }

        private void backbtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win1 = new MainWindow();
            win1.Show();
            this.Close();
        }
    }
}
