﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Conference
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        public void Show(ref Attendee p1)
        {
            Label my_label = new Label();
            my_label.Name = "my_label";
            string the_label = String.Format ("Invoice For {0} {1} attended  {2} and paid {3:C2}", p1.Fname, p1.Sname, p1.Cname, p1.GetCost());
            my_label.Content = the_label;

            Grid.SetRow(my_label, 0);
            Grid.SetColumn(my_label, 0);
            Invoicegrid.Children.Add(my_label);
            base.Show();
        }

        private void closebtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win1 = new MainWindow();
            win1.Show();
            this.Close();


        }
    }
}
